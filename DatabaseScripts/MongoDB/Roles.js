﻿Roles = [
    {
        "RoleName": "Administrators",
        "Description": "系统管理员",
        "Menus": [],
        "Apps": []
    },
    {
        "RoleName": "Default",
        "Description": "默认用户，可访问前台页面",
        "Menus": [],
        "Apps": []
    }
];